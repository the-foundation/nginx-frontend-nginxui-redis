#!/bin/bash

## functions
randomsleepfive() {  sleep $(($RANDOM%5)) ; } ;

check_nx_domain() {
  dom="$1"
  (host "$dom" 1.1.1.1 2>&1 ;host "$dom" 8.8.8.8  2>&1 ;host "$dom" 4.2.2.4  2>&1 ) |sort -u|grep NXDOMAIN
}

check_ssl_21days_host() {
  openssl x509 -checkend $(( 24*3600*21 )) -noout -in <(openssl s_client -showcerts -connect my.domain.com:443 </dev/null 2>/dev/null | openssl x509 -outform PEM)
  if [ $? -eq 0 ]; then
    echo OK;
  else
    echo RENEW;
  fi
}

check_ssl_21days_file() {
  openssl x509 -checkend $(( 24*3600*21 )) -noout -in "$1" 2>/dev/null
  if [ $? -eq 0 ]; then
    echo OK;
  else
    echo RENEW;
  fi
}


## dying certbot leaves an include that HAS to exist
grep /etc/letsencrypt/le_http_01_cert_challenge.conf /etc/nginx/nginx.conf && (
test -e /etc/letsencrypt/le_http_01_cert_challenge.conf || touch /etc/letsencrypt/le_http_01_cert_challenge.conf

)

#letsencrypt expects the service file to exist
which service || (
(echo  '#!/bin/bash';echo 'echo "$@" > /dev/shm/.lastservicecall';echo 'echo "$@"|grep -q -e "^nginx reload" -e "^nginx restart" && (nginx -t &>/dev/null && nginx -s reload &>/dev/null && echo nginx reload ) ') > /sbin/service
chmod +x /sbin/service
)
trap 'date > /dev/shm/.nginxkilled & test -e /dev/shm/.nginxkilled || echo nginx wrapper suicide;kill 0 ;kill -9 -- $$  ; exit;'  SIGINT SIGTERM INT TERM
mkdir -p /dev/shm/adblock/
echo "AD BLOCKED" > /dev/shm/adblock/index.html
while (true);do redis-server;sleep 2;done &
sleep 0.2
mkdir -p /var/www /var/log/nginx
[[ -z "$GITURL_WEBROOT" ]] || git clone $GITURL_WEBROOT /var/www/html
test -e /var/www/html || /bin/mkdir -p /var/www/html ;

mkdir -p /var/lib/letsencrypt

chgrp www-data /etc/nginx -R ;chmod g+r /etc/nginx
chmod g+x /etc/nginx

test -e /var/lib/nginx/logs/    &&  (
    chgrp -R www-data /var/lib/nginx/logs/
    chgrp www-data /var/lib/nginx/
    chmod g+x      /var/lib/nginx/logs/
    chmod g+rw -R  /var/lib/nginx/logs/
    )

test -e /var/log/letsencrypt/   &&  chown -R www-data:www-data /var/log/letsencrypt/
test -e /var/lib/letsencrypt/   &&  chown -R www-data:www-data /var/lib/letsencrypt/
test -e /etc/letsencrypt/       &&  chown -R www-data:www-data /etc/letsencrypt/
#test -e /var/run/php  || mkdir /var/run/php
#chown -R www-data /var/run/php

#user supplied config
test -e /etc/nginx/conf.d/zzz999-default.catchall.letsencrypt.conf && (  diff -q /etc/nginx/conf.d/zzz999-default.catchall.letsencrypt.conf /etc/nginx/sites-enabled/zzz999-default.catchall.letsencrypt.conf &>/dev/null || ( echo "USER SUPPLIED CATCHALL";mv /etc/nginx/sites-enabled/zzz999-default.catchall.letsencrypt.conf /tmp/ ) )


## switching RSA/ECDSA needs forced renewals
test -e /usr/bin/cert-force-renew-rsa4096 || (
    (
       echo '#!/bin/bash' ;
      # echo 'su -s /bin/bash www-data -c "/usr/bin/certbot renew  --force-renewal --agree-tos --no-random-sleep-on-renew --logs-dir /dev/shm/ --preferred-challenges http-01,dns-01 --key-type ecdsa --elliptic-curve secp384r1 -a nginx 2>&1 "  |grep -v -e '\[warn\]' -e ^- -e ^$ -e "not yet due" -e "skipped" -e ^/ -e ^Processing  -e "are not due for renewal" -e "No renewals were attempted" -e "Saving debug log to" 2>&1 | sed "s/^/letsencrypt:/g" | tee /dev/shm/letsencrypt.log >&2'
       echo '/usr/bin/certbot renew  --force-renewal --agree-tos --no-random-sleep-on-renew --logs-dir /dev/shm/ --preferred-challenges http-01,dns-01 --key-type rsa --rsa-key-size 4096           -a nginx $@ 2>&1   |grep -v -e "\[warn\]" -e ^- -e ^$ -e "not yet due" -e "skipped" -e ^/ -e ^Processing  -e "are not due for renewal" -e "No renewals were attempted" -e "Saving debug log to" 2>&1 | sed "s/^/letsencrypt:/g" | tee /dev/shm/letsencrypt.log >&2'
    ) > /usr/bin/cert-force-renew-rsa4096 ;chmod +x /usr/bin/cert-force-renew-rsa4096 )

test -e /usr/bin/cert-force-renew-secp384r1 || (
    (
       echo '#!/bin/bash' ;
      # echo 'su -s /bin/bash www-data -c "/usr/bin/certbot renew  --force-renewal --agree-tos --no-random-sleep-on-renew --logs-dir /dev/shm/ --preferred-challenges http-01,dns-01 --key-type ecdsa --elliptic-curve secp384r1 -a nginx 2>&1 "  |grep -v -e '\[warn\]' -e ^- -e ^$ -e "not yet due" -e "skipped" -e ^/ -e ^Processing  -e "are not due for renewal" -e "No renewals were attempted" -e "Saving debug log to" 2>&1 | sed "s/^/letsencrypt:/g" | tee /dev/shm/letsencrypt.log >&2'
       echo '/usr/bin/certbot renew  --force-renewal --agree-tos --no-random-sleep-on-renew --logs-dir /dev/shm/ --preferred-challenges http-01,dns-01 --key-type ecdsa --elliptic-curve secp384r1 -a nginx  $@ 2>&1   |grep -v -e "\[warn\]" -e ^- -e ^$ -e "not yet due" -e "skipped" -e ^/ -e ^Processing  -e "are not due for renewal" -e "No renewals were attempted" -e "Saving debug log to" 2>&1 | sed "s/^/letsencrypt:/g" | tee /dev/shm/letsencrypt.log >&2'
    ) > /usr/bin/cert-force-renew-secp384r1 ;chmod +x /usr/bin/cert-force-renew-secp384r1 )


#test -d /tmp/git ||  git clone  /tmp/git 2>&1 || exit 2  |grep -v -e 'warning: redirecting'  |tr -d '\n'  ;
test -e /var/lib/nginx/tmp || (mkdir -p /var/lib/nginx/tmp && chown -R www-data /var/lib/nginx/tmp)
# && wc -l /var/www/html/store.php |grep -q ^0 && cat /var/www/store.init.php > /var/www/html/store.php;
#xwhile (true);do su -s /bin/bash -c "php-fpm7 -F" www-data;sleep 1;done &
#while (true);do php-fpm7 -F;sleep 1;done &
test -e /var/www/000_default/.well-known || mkdir -p /var/www/000_default/.well-known

chown -R www-data:www-data /etc/letsencrypt /var/www/var/html/ /var/www /var/log/nginx/ /var/lib/nginx/tmp/ /var/www/000_default/.well-known

chown -R www-data:www-data /etc/letsencrypt /var/www/var/html/ /var/www /var/log/nginx/ /var/lib/nginx/tmp/
#test -e /etc/sites-enabled || ln -s /etc/nginx/conf.d /etc/nginx/sites-enabled
mkdir /run/nginx
chmod a+rw /run/nginx
mkdir -p /etc/nginx/conf.d
test -e /etc/sites-enabled || mkdir -p /etc/nginx/sites-enabled
test -e /etc/nginx/ssl/    || mkdir -p /etc/nginx/ssl/
( test -e /etc/nginx/ssl/dhparam.pem || openssl dhparam -out /etc/nginx/ssl/dhparam.pem -dsaparam 4096 ) &
test -e /etc/nginx/nginx.conf || cp /etc/nginx.default.conf /etc/nginx/nginx.conf
#test -e /etc/nginx/ssl/selfsigned_fullchain.pem || ln -s

#rm /etc/ssl/certs/ssl-cert-snakeoil.pem /etc/nginx/ssl/selfsigned_fullchain.pem
#/etc/ssl/private/ssl-cert-snakeoil.key /etc/ssl/private/nginx-selfsigned.key

#test -e /etc/nginx/mime.types || cp /etc/nginx_alpine_original/mime.types /etc/nginx/
( cd /etc_nginx_default/ && for file in * ;do test -e /etc/nginx/$file||cp -aurv "$file" /etc/nginx/;done )

export SYSTEM_CERT=/etc/nginx/ssl/selfsigned_fullchain.pem

bash /_0_crt-snakeoil.sh

ln -s /etc/nginx/ssl/selfsigned_fullchain.pem /etc/ssl/private/fullchain.pem

#ln -s /etc/ssl/private/ssl-cert-snakeoil.key  /etc/ssl/private/nginx-selfsigned.key
#ln -s /etc/ssl/private/ssl-cert-snakeoil.key  /etc/ssl/private/privkey.pem
#
#ln -s /etc/ssl/private/ssl-cert-snakeoil.pem  /etc/ssl/certs/nginx-selfsigned.crt
#ln -s /etc/ssl/private/ssl-cert-snakeoil.pem


#RUN openssl req -x509 -nodes -days 36500 -subj "/C=CA/ST=QC/O=NoCorp, Inc./CN=nodomain.lan" -addext "subjectAltName=DNS:nodomain.lan" -newkey rsa:4096 -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt;
#RUN cat /etc/ssl/private/nginx-selfsigned.key /etc/ssl/certs/nginx-selfsigned.crt > /etc/ssl/private/fullchain.pem
#RUN cat /etc/ssl/private/nginx-selfsigned.key > /etc/ssl/private/privkey.pem

cat /etc/nginx/ssl/ca/rootCA.crt  > /dev/shm/adblock/ca.crt
test -e /etc/nginx/ssl/.selfsigned_fullchain.pem.hasca || (  cat /etc/nginx/ssl/ca/rootCA.crt >> /etc/nginx/ssl/selfsigned_fullchain.pem ; touch /etc/nginx/ssl/.selfsigned_fullchain.pem.hasca)
#ln -s /etc/nginx/ssl/selfsigned_fullchain.pem /etc/ssl/certs/ssl-cert-snakeoil.pem
test -e /etc/ssl/certs/ssl-cert-snakeoil.pem || ( ln -s  /etc/nginx/ssl/selfsigned_fullchain.pem /etc/ssl/private/ssl-cert-snakeoil.pem ;
                                                  ln -sf /etc/ssl/private/ssl-cert-snakeoil.key /etc/ssl/private/nginx-selfsigned.key   )

test -e /etc/ssl/private/fullchain.pem || ln -s /etc/ssl/private/ssl-cert-snakeoil.pem /etc/ssl/private/fullchain.pem
test -e /etc/ssl/private/privkey.pem   || ln -s /etc/ssl/private/ssl-cert-snakeoil.key /etc/ssl/private/privkey.pem

test -e /etc/nginx/snippets ||mkdir /etc/nginx/snippets
ls -1 /etc/nginx/http.d/* |wc -l |grep ^0$ && ( cp /etc/example-configs/000.0-default-variables-logformats.conf /etc/nginx/snippets/000.0-default-variables-logformats.conf;
                                      test -e /etc/nginx/conf.d/zzz999-default.catchall.letsencrypt.conf ||   cp /etc/example-configs/zzz999-default.catchall.letsencrypt.conf /etc/nginx/conf.d/)

for file in $( echo /etc/ssl/private
               echo /etc/ssl/private/privkey.pem
               echo /etc/ssl/private/fullchain.pem
               echo /etc/ssl/certs/ssl-cert-snakeoil.pem
               echo /etc/ssl/private/ssl-cert-snakeoil.pem
               echo /etc/ssl/private/ssl-cert-snakeoil.key
               echo /etc/nginx/ssl/selfsigned_fullchain.pem
               echo /etc/ssl/private/nginx-selfsigned.key );do
    test -e "$file" && chown -R nginx:www-data "$file"
    test -e "$file" && chmod g+r "$file"

done
mkdir -p /run/nginx/
chown www-data /run/nginx/

( mkdir /home/www-data ; chown  www-data:www-data /home/www-data ) &


mkdir /dev/shm/letsencrypt.init
echo "INIT"

## generation BLOCK

[[ -z "$KEYTYPE" ]] && KEYTYPE=ECDSA

#RSA4096
echo $KEYTYPE |grep -q "RSA" && (

sleep 23
echo "CERTBOT:RENEW"
ls -1 /etc/letsencrypt/live/|grep -v README|while read dom;do
    type=ECDSA;openssl x509 -in /etc/letsencrypt/live/$dom/cert.pem -text|grep Key |grep  -q 'RSA Public-Key: (' && type=RSA;
    echo -ne "|"$type'\t' ; echo "|VALID:"$(openssl x509 -in /etc/letsencrypt/live/$dom/cert.pem -text|grep 'Not After : ')"|"$dom ;
    dorenew=no
    [[ "$KEYTYPE" = "$type" ]] || dorenew=yes
    [[ "$KEYTYPE" = "$type" ]] || echo "KEY_TYPE_CHANGED TO $KEYTYPE"
    check_ssl_21days_file /etc/letsencrypt/live/$dom/cert.pem |grep -q RENEW && echo "KEY EXPIRES IN UNDER 21 DAYS"
    check_ssl_21days_file /etc/letsencrypt/live/$dom/cert.pem |grep -q RENEW && dorenew=yes
    check_nx_domain "$dom" |grep NXDOMAIN && dorenew=no
    echo "$dorenew"|grep -q yes && (
      check_nx_domain "$dom" |grep NXDOMAIN || /usr/bin/certbot renew  --force-renewal --agree-tos --no-random-sleep-on-renew --logs-dir /dev/shm/letsencrypt.init --preferred-challenges http-01,dns-01 --key-type rsa --rsa-key-size 4096 -a nginx --cert-name "$dom" 2>&1   |grep -v -e "\[warn\]" -e ^- -e ^$ -e "^Processing" -e "not yet due" -e "skipped" -e ^/  -e "are not due for renewal" -e "No renewals were attempted" -e "Saving debug log to" 2>&1 | sed "s/^/letsencrypt:/g" | tee -a  /dev/shm/letsencrypt.init.log
      randomsleepfive
       )
done

ln -s /usr/bin/cert-force-renew-rsa4096 /usr/bin/cert-force-renew
cat /usr/bin/cert-force-renew |sed 's/--force-renewal//g' > /usr/bin/cert-renew
chmod +x /usr/bin/cert-renew
sleep 90;
while (true);do
    sleep 60;
    inotifywatch -e modify -e create /etc/nginx 2>&1|grep -v atch;
    newcert() {
        [[ -z "$LETSENCRYPT_EMAIL" ]] && export LETSENCRYPT_EMAIL="default-mail-not-set@using-fallback-default.slmail.me";
        /usr/bin/certbot certonly --agree-tos --logs-dir /dev/shm/ --email $LETSENCRYPT_EMAIL  --preferred-challenges http-01,dns-01 --rsa-key-size 4096 --webroot -w  /var/www/000_default/ -d "$@" --cert-name $1  ; } ;
        nginx -T|sed 's/$/ /g'|tr -d '\n'|sed 's/server /\nserver /g'|grep 443 |while read srv;do
            names=$(echo "$srv"|sed 's/;/\n/g;s/.\+#.\+server_name//g'|grep server_name|sed 's/.\+server_name //g'|grep -v '*');
            [[ -z "$names" ]] || {
                certname=${names/ */} ;
                test -e /etc/letsencrypt/live/$certname/fullchain.pem || newcert $names ;
                test -e /etc/letsencrypt/live/$certname/fullchain.pem && grep /etc/letsencrypt/live/$certname/fullchain.pem /etc/nginx/conf.d/* -q || (
                (echo "# key /etc/letsencrypt/live/$certname/privkey-pem";echo "# cert /etc/letsencrypt/live/$certname/fullchain.pem" ) >>/etc/nginx/conf.d/$name.conf ) ; } ;
        done ;
        nginx -t &>/dev/null && nginx -s reload &>/dev/null ;
        done &

while (true);do
    #su -s /bin/bash www-data -c "/usr/bin/certbot renew --no-random-sleep-on-renew --agree-tos --logs-dir /dev/shm/ --preferred-challenges http-01,dns-01 --rsa-key-size 4096 -a nginx 2>&1 " |grep -v -e '\[warn\]' -e ^- -e ^$ -e "not yet due" -e "skipped" -e ^/ -e ^Processing  -e "are not due for renewal" -e "No renewals were attempted" -e "Saving debug log to" 2>&1 | sed 's/^/letsencrypt:/g' | tee /dev/shm/letsencrypt.log >&2 ;sleep 43000;
    /usr/bin/certbot renew --no-random-sleep-on-renew --agree-tos --logs-dir /dev/shm/ --preferred-challenges http-01,dns-01 --rsa-key-size 4096 -a nginx 2>&1  |grep -v -e '\[warn\]' -e ^- -e ^$ -e "not yet due" -e "skipped" -e ^/ -e ^Processing  -e "are not due for renewal" -e "No renewals were attempted" -e "Saving debug log to" 2>&1 | sed 's/^/letsencrypt:/g' | tee /dev/shm/letsencrypt.log >&2 ;
    cat /dev/shm/letsencrypt.log |tail -n 30 |grep -e renewals -e "renewal failures" -e "successful renew" -e "renewal success" -e "renewals succes"
    sleep 43000;
  done &
) &

echo $KEYTYPE |grep -q "ECDSA" && (
ln -s /usr/bin/cert-force-renew-secp384r1 /usr/bin/cert-force-renew
cat /usr/bin/cert-force-renew |sed 's/--force-renewal//g' > /usr/bin/cert-renew
chmod +x /usr/bin/cert-renew
sleep 23
echo "CERTBOT:RENEW"
ls -1 /etc/letsencrypt/live/|grep -v README|while read dom;do
    type=ECDSA;openssl x509 -in /etc/letsencrypt/live/$dom/cert.pem -text|grep Key |grep  -q 'RSA Public-Key: (' && type=RSA;
    echo -ne "|"$type'\t' ; echo "|VALID:"$(openssl x509 -in /etc/letsencrypt/live/$dom/cert.pem -text|grep 'Not After : ')"|"$dom ;
    dorenew=no
    [[ "$KEYTYPE" = "$type" ]] || dorenew=yes
    [[ "$KEYTYPE" = "$type" ]] || echo "KEY_TYPE_CHANGED TO $KEYTYPE"
    check_ssl_21days_file /etc/letsencrypt/live/$dom/cert.pem |grep -q RENEW && echo "KEY EXPIRES IN UNDER 21 DAYS"
    check_ssl_21days_file /etc/letsencrypt/live/$dom/cert.pem |grep -q RENEW && dorenew=yes
    check_nx_domain "$dom" |grep NXDOMAIN && dorenew=no
    echo "$dorenew"|grep -q yes && (
      check_nx_domain "$dom" | grep NXDOMAIN||/usr/bin/certbot renew  --force-renewal --agree-tos --no-random-sleep-on-renew --no-random-sleep-on-renew --logs-dir /dev/shm/letsencrypt.init --preferred-challenges http-01,dns-01 --key-type ecdsa --elliptic-curve secp384r1 -a nginx --cert-name "$dom" 2>&1   |grep -v -e "\[warn\]" -e ^- -e ^$ -e "^Processing" -e "not yet due" -e "skipped" -e ^/  -e "are not due for renewal" -e "No renewals were attempted" -e "Saving debug log to" 2>&1 | sed "s/^/letsencrypt:/g" | tee -a  /dev/shm/letsencrypt.init.log
      randomsleepfive
       )
done
sleep 90;
while (true);do
    sleep 60;
    inotifywatch -e modify -e create /etc/nginx 2>&1|grep -v atch;
    newcert() {
        [[ -z "$LETSENCRYPT_EMAIL" ]] && export LETSENCRYPT_EMAIL="default-mail-not-set@using-fallback-default.slmail.me";
        /usr/bin/certbot certonly --agree-tos --logs-dir /dev/shm/ --email $LETSENCRYPT_EMAIL  --preferred-challenges http-01,dns-01 --key-type ecdsa --elliptic-curve secp384r1 --webroot -w  /var/www/000_default/ -d "$@" --cert-name $1  ; } ;
        nginx -T 2>/dev/null |sed 's/$/ /g'|tr -d '\n'|sed 's/server /\nserver /g'|grep 443 |while read srv;do
            names=$(echo "$srv"|sed 's/;/\n/g;s/.\+#.\+server_name//g'|grep server_name|sed 's/.\+server_name //g'|grep -v '*');
            [[ -z "$names" ]] || {
                certname=${names/ */} ;
                test -e /etc/letsencrypt/live/$certname/fullchain.pem || newcert $names ;
                test -e /etc/letsencrypt/live/$certname/fullchain.pem && grep /etc/letsencrypt/live/$certname/fullchain.pem /etc/nginx/conf.d/* -q || (
                (echo "# key /etc/letsencrypt/live/$certname/privkey-pem";echo "# cert /etc/letsencrypt/live/$certname/fullchain.pem" ) >>/etc/nginx/conf.d/$name.conf ) ; } ;
        done ;
        nginx -t &>/dev/null && nginx -s reload &>/dev/null ;
        done &

while (true);do
    #su -s /bin/bash www-data -c  "/usr/bin/certbot renew --no-random-sleep-on-renew --agree-tos --logs-dir /dev/shm/ --preferred-challenges http-01,dns-01 --key-type ecdsa --elliptic-curve secp384r1 -a nginx 2>&1 "  |grep -v -e '\[warn\]' -e ^- -e ^$ -e "not yet due" -e "skipped" -e ^/ -e ^Processing  -e "are not due for renewal" -e "No renewals were attempted" -e "Saving debug log to" 2>&1 | sed 's/^/letsencrypt:/g' | tee /dev/shm/letsencrypt.log >&2 ;sleep 43000;
    /usr/bin/certbot renew --no-random-sleep-on-renew --agree-tos --logs-dir /dev/shm/ --preferred-challenges http-01,dns-01 --key-type ecdsa --elliptic-curve secp384r1 -a nginx 2>&1   |grep -v -e '\[warn\]' -e ^- -e ^$ -e "not yet due" -e "skipped" -e ^/ -e ^Processing  -e "are not due for renewal" -e "No renewals were attempted" -e "Saving debug log to" 2>&1 | sed 's/^/letsencrypt:/g' | tee /dev/shm/letsencrypt.log >&2 ;
    cat /dev/shm/letsencrypt.log |tail -n 30 |grep -e renewals -e "renewal failures" -e "successful renew" -e "renewal success" -e "renewals succes"
    sleep 43000;
  done &
) &
#


#sleep 2;
touch          /run/nginx/nginx.pid
chown www-data /run/nginx/nginx.pid
touch          /run/nginx.pid
chown www-data /run/nginx.pid
chgrp www-data /dev/stderr /dev/stdout

while (true);do nginx -t  &>/dev/null && nginx -g 'daemon off;' -c /etc/nginx/nginx.conf; sleep 1;done

#while (true);do sleep 120 ;cd /tmp/git ;
#wc -l  /var/www/html/pub/wiki.html |grep -q ^0 || cat /var/www/html/pub/wiki.html >  /tmp/git/wiki.html ;
#      cp -aur /var/www/html/pub/backup/* /tmp/git/backup ;
#      find /tmp/git/backup/ -type f -name '*.html' |sort -nr|tail -n+10 |grep html$|grep /tmp/git| xargs rm ;
#      config user.name ;
#      git config user.email ;
#      git add -A 2>&1 | grep -v -e ^$ -e 'nothing to commit, working tree clean' -e ^'Everything up-to-date' -e 'On branch master' -e 'Your branch is up to date with' -e 'warning: redirecting'  |tr -d '\n' ;
#      git commit -m $(date +%s) 2>&1 | grep -v -e 'nothing to commit, working tree clean' -e ^'Everything up-to-date' -e 'Your branch is up to date with' -e 'warning: redirecting'  |tr -d '\n';
#      git push  2>&1  | grep -v -e ^$ -e 'nothing to commit, working tree clean' -e ^'Everything up-to-date' -e 'On branch master' -e 'Your branch is up to date with' -e 'warning: redirecting'  |tr -d '\n' ;done &
#wait
#echo starting
#exec nginx -g 'daemon off;'
