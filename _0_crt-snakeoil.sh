#!/bin/bash
echo "SNAKEOIL CERT:"


[[ -z "$CUSTOM_CRT" ]]  && CUSTOM_CRT=/etc/ssl/private/ssl-cert-snakeoil.pem
[[ -z "$PRIVATE_KEY" ]] && PRIVATE_KEY=/etc/ssl/private/ssl-cert-snakeoil.key
[[ -z "$SYSTEM_CERT" ]] && SYSTEM_CERT=/etc/ssl/certs/ssl-cert-snakeoil.pem

## letsencrypt provides privkey.pem and fullchain.pem

# use it like a volume in dockerfile
# - /etc/nginx/certs/${APP_URL}/:/etc/ssl/private_letsencrypt:ro


LE_FULL=/etc/ssl/private_letsencrypt/fullchain.pem
LE_PRIV=/etc/ssl/private_letsencrypt/key.pem


#test -f /etc/ssl/certs/ssl-cert-snakeoil.pem && test -f /etc/ssl/private/ssl-cert-snakeoil.key || openssl req -new -x509 -days 365 -nodes -out /etc/ssl/certs/ssl-cert-snakeoil.pem -keyout /etc/ssl/private/ssl-cert-snakeoil.key &

_is_cert() { grep -q "BEGIN CERTIFICATE--" "$1" && grep -q "END CERTIFICATE--" "$1" ; } ;
_is_key()  {
             grep -q -e "BEGIN PRIVATE KEY--" -e "BEGIN RSA PRIVATE KEY--" "$1" &&  grep -q -e "END RSA PRIVATE KEY--" -e "END PRIVATE KEY--" "$1" ; } ;
_have_snakeoil() {  
             test -f ${SYSTEM_CERT} && test -f ${PRIVATE_KEY} && _is_key ${PRIVATE_KEY} && _is_cert ${SYSTEM_CERT} ; } ;
             test -f ${LE_PRIV} && _is_key ${LE_PRIV} &&  test -f ${LE_PRIV} && _is_cert ${LE_FULL} && {
                   echo "using letsencrypt certs as softlink"
             ln -sf ${LE_PRIV} ${PRIVATE_KEY}
             ln -sf ${LE_FULL} ${SYSTEM_CERT}


  which inotifywait 2>/dev/null |grep -q inotifywait && {
    echo use:inotifywait Letsencrypt Watcher
    which nginx 2>/dev/null |grep -q nginx && while inotifywait -q -e close_write ${LE_FULL} ${LE_PRIV}; do nginx -t && nginx -s reload;sleep 0.3; done &
    which apache2 2>/dev/null |grep -q apache2 && while inotifywait -q -e close_write ${LE_FULL} ${LE_PRIV}; do apache2ctl configtest|grep "Syntax OK"  -q && service apache2 reload;sleep 0.3; done &
  }
  which inotifywait 2>/dev/null |grep -q inotifywait || {
    echo use:sha512sum Letsencrypt Watcher
    sumcr=$(sha512sum  "${LE_PRIV}" )
    sumky=$(sha512sum  "${LE_FULL}" )
    while (true) ;do
      sleep 300
      ## checksum letsencrypt certs
      tmp_sumcr=$(sha512sum  "${LE_PRIV}" )
      tmp_sumky=$(sha512sum  "${LE_FULL}" )
      keys_match=OK
      [[ "${tmp_sumcr}" = "${sumcr}" ]] || keys_match=no
      [[ "${tmp_sumky}" = "${sumky}" ]] || keys_match=no
      ## reload webserver on ssl cert changes
      [[ "${keys_match}" = "no" ]] && {
        which nginx 2>/dev/null |grep -q nginx &&  nginx -t && nginx -s reload
        which apache2 2>/dev/null |grep -q apache2 &&  apache2ctl configtest|grep "Syntax OK"  -q && service apache2 reload
      echo -n ; } ;
     done &
  }

}

_have_snakeoil || {
    echo -n "SSL Certs:FAILED 1" >&2
    rm ${SYSTEM_CERT} ${PRIVATE_KEY} 2>/dev/null   ; } ;


mkdir -p $(dirname "${CUSTOM_CRT}")
mkdir -p $(dirname "${SYSTEM_CERT}")
_have_snakeoil ||  (which  make-ssl-cert >&/dev/null &&  make-ssl-cert generate-default-snakeoil --force-overwrite ) &
##if make-ssl-certs is missing..
which  make-ssl-cert >&/dev/null || which openssl &>/dev/null && _have_snakeoil || (
echo "CREATING SELFSIGNED via openssl" >&2 ;
mkdir /etc/nginx/ssl/ca -p ;cd /etc/nginx/ssl/ca;

echo "#Create Root Key"
openssl genrsa -out rootCA.key 4096
echo "#Create and self sign the Root Certificate"
openssl req -x509 -new -nodes -key /etc/nginx/ssl/ca/rootCA.key -subj "/C=XX/ST=XX/O=CatchAll-AdBlock CA, Inc./CN=ca.blocked" -sha256 -days 36500 -out /etc/nginx/ssl/ca/rootCA.crt -addext "subjectAltName = DNS:ca.blocked.lan,DNS:*.ca.blocked"
#                  -addext "certificatePolicies = 1.2.3.4" \
ls
echo '# server_rootCA.csr.cnf
[req]
default_bits = 4096
prompt = no
default_md = sha256
distinguished_name = dn

[dn]
C=XX
ST=XY
L=World
O=CatchAll-AdBlock CA
OU=local_rootCA
emailAddress=ca@blocked.lan
CN = ca.blocked'  > /dev/shm/srv.ext
echo '# v3.ext
authorityKeyIdentifier=keyid,issuer
basicConstraints=CA:FALSE
keyUsage = digitalSignature, nonRepudiation, keyEncipherment, dataEncipherment
subjectAltName = @alt_names

[alt_names]
' > /dev/shm/v3.ext

echo 'DNS:*.blocked.lan,DNS:*.com,DNS:*.uk,DNS:*.uk,DNS:*.net,DNS:*.de,DNS:*.org,DNS:*.io,DNS:*.xyz,DNS:*.dk,DNS:*.cn,DNS:*.au,DNS:*.us,DNS:*.ml,DNS:*.to,DNS:*.lan,DNS:*.local'|grep -v ^$|sed 's/DNS/\n/g'|grep -v ^$|cut -d":" -f2|nl|sed 's/^/DNS./g;s/*/=*/g'|tr -d ' \t,'|sed 's/=/ = /g' >> /dev/shm/v3.ext
nl /dev/shm/v3.ext

echo "req new"
#test -e ${PRIVATE_KEY} || openssl genrsa -out ${PRIVATE_KEY} 4096
test -e ${PRIVATE_KEY} || openssl req -new -sha256 -nodes -out catchall.csr -newkey rsa:4096 -keyout ${PRIVATE_KEY} -config <( cat /dev/shm/srv.ext )
echo "x509 req"
#test -e ${SYSTEM_CERT} || openssl req -new -sha256 -key ${PRIVATE_KEY} -subj "/C=XX/ST=XX/O=CatchAll-AdBlock, SRV/CN=ad.blocked" -out /etc/nginx/ssl/ca/catchall.csr 
test -e ${SYSTEM_CERT} ||  openssl x509 -req -in catchall.csr -CA /etc/nginx/ssl/ca/rootCA.crt -CAkey /etc/nginx/ssl/ca/rootCA.key -CAcreateserial -out ${SYSTEM_CERT}  -days 3650 -sha256 -extfile /dev/shm/v3.ext

#openssl req -new -sha256 -key ${PRIVATE_KEY} -subj "/C=XX/ST=XX/O=CatchAll-AdBlock, SRV/CN=ad.blocked" -out /etc/nginx/ssl/ca/catchall.csr 
# -addext "subjectAltName = DNS:blocked.lan,DNS:*.com,DNS:*.uk,DNS:*.uk,DNS:*.net,DNS:*.de,DNS:*.org,DNS:*.io,DNS:*.xyz,DNS:*.dk,DNS:*.cn,DNS:*.au,DNS:*.us,DNS:*.ml,DNS:*.to,DNS:*.lan,DNS:*.local" 
#test -e ${SYSTEM_CERT} || openssl req -in /etc/nginx/ssl/ca/catchall.csr -noout -text
# -addext "subjectAltName = DNS:blocked.lan,DNS:*.com,DNS:*.uk,DNS:*.uk,DNS:*.net,DNS:*.de,DNS:*.org,DNS:*.io,DNS:*.xyz,DNS:*.dk,DNS:*.cn,DNS:*.au,DNS:*.us,DNS:*.ml,DNS:*.to,DNS:*.lan,DNS:*.local" 
#openssl x509 -req -in catchall.csr -CA /etc/nginx/ssl/ca/rootCA.crt -CAkey /etc/nginx/ssl/ca/rootCA.key -CAcreateserial -out ${SYSTEM_CERT}  -days 3650 -sha256 -extensions req_ext -extfile /dev/shm/v3.ext
)
#(sleep 0.3;echo;echo;echo;echo;echo;echo;echo;echo;echo)|openssl req -x509 -sha256  -newkey rsa:4096   -days 32768 -nodes -out ${SYSTEM_CERT} -keyout ${PRIVATE_KEY} ) &


wait
_have_snakeoil || echo -n "CRITICAL ERROR: SSL Certs:FAILED after regen" >&2
