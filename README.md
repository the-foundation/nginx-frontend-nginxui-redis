nginx with redis and nginxui docker-compose
===

## FEATURES:

* detects new nginx config files
   * auto-injects a self signed cert first
   * tries to issue a valid letsencrypt cert
* example configs in `nginx-example-configs`
* generates secure Keys by default ( `RSA4096` | `ECDSA-secp-384r1` )
## QUICK START:
  * git clone this project , create `.env` ( example in DOTenv.example )
  * `docker-compose up -d --build`  from inside directory

### Overview

![Network Overview](./net.png)


---

## MAINTAINANCE
##### Attention: renewals etc. are automated and you might run into account limits first check the logs , every 43000 seconds (½ Day) the log should contain lines like:

```
2023-03-10 05:29:41,904:DEBUG:certbot._internal.display.obj:Notifying user: No renewals were attempted.
2023-03-10 05:29:41,905:DEBUG:certbot._internal.renewal:no renewal failures
```
### MANUAL RENEWAL ( DANGER , rate limits apply when forcing and re-issuing)

* **standard** renewal `/usr/bin/cert-renew`       , results in `/dev/shm/letsencrypt.log`
* **FORCED**   renewal `/usr/bin/cert-force-renew` , results in `/dev/shm/letsencrypt.log`

### renewal examples:
   * `docker exec -it nginx /usr/bin/cert-force-renew`
   * `docker exec -it nginx /usr/bin/cert-renew`
   * ( **in both above cases** you can see the log with `docker logs nginx`)
   * your container might not be named `nginx`, so **adjust commands if necessary**

---

## INSTALLATION

Regular Systems:
* `git clone https://gitlab.com/the-foundation/nginx-frontend-nginxui-redis.git myfolder`
* create a `.env` file in this folder
* `docker-compose up -d` from this folder

if you have no git installed but docker:
* create the desired compose folder
* `cd` to it
* run the following command
```
docker run --rm -it -v $PWD:$PWD git ash -c " git clone https://gitlab.com/the-foundation/nginx-frontend-nginxui-redis.git $PWD --recurse-submodules;cd $PWD;pwd; git pull --recurse-submodules;git status ";docker-compose build  ;docker-compose up -d
```

---

### SCREENSHOTS

* Startup example after KEYTYPE change from `RSA` to `ECDSA`:
   ![ssl renewal after key type change](./screenshot_init.png)
---


<h3>A project of the foundation</h3>
<a href="https://the-foundation.gitlab.io/"><div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/certbot-host-europe-sshfs-telegram/README.md/logo.jpg" width="480" height="270"/></div></a>
