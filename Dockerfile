FROM thefoundation/upgraded-operating-systems:alpine
#RUN apk update && apk upgrade && apk add git lsof curl bash php7 php7-fpm php7-opcache && apk add php7-gd php7-mysqli php7-zlib php7-curl php7-sqlite3 nginx openssl

RUN apk update && apk upgrade && apk add lsof git iftop htop iotop bash curl openssh-client jq apache2-utils nano openssl dropbear redis certbot certbot-nginx procps bind-tools iftop
RUN apk add $(apk list|grep nginx|grep mod|grep -e headers-more -e fancy  -e upstream -e encrypt -e accounting -e geoip -e zip -e cache-purge -e echo -e untar -e upload-progress -e rtmp -e dav-ext -e brotli  -e redis -e cookie-flag -e set-misc -e mail -e nginx-mod-stream |sed 's/-[0-9].\+//g')
# ensure www-data user exists
#RUN set -x ; addgroup -g 82 -S www-data ;  adduser -u 82 -D -S -G www-data www-data && exit 0 ; exit 1
RUN deluser  xfs || true && delgroup xfs|| true && deluser  www-data || true && delgroup www-data || true &&  grep 33 /etc/group||true && grep 33 /etc/passwd ||true
RUN  addgroup -g 33        -S www-data && adduser  -u 33 -D -S -G www-data www-data
RUN /bin/mkdir -p /var/run/nginx || true && /bin/mkdir -p /etc/ssl/private/||true
#RUN openssl req -x509 -nodes -days 36500 -subj "/C=CA/ST=QC/O=NoCorp, Inc./CN=nodomain.lan" -addext "subjectAltName=DNS:nodomain.lan" -newkey rsa:4096 -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt;
#RUN cat /etc/ssl/private/nginx-selfsigned.key /etc/ssl/certs/nginx-selfsigned.crt > /etc/ssl/private/fullchain.pem
#RUN cat /etc/ssl/private/nginx-selfsigned.key > /etc/ssl/private/privkey.pem
COPY etc_nginx_default/ /etc_nginx_default/
COPY nginx.default.conf /etc/nginx.default.conf
COPY nginx-example-configs /etc/example-configs

RUN cp -r /etc/nginx /etc/nginx_alpine_original
RUN rm -rf /etc/nginx/sites-enabled || true && ln -s /etc/nginx/conf.d /etc/nginx/sites-enabled
COPY _0_crt-snakeoil.sh /_0_crt-snakeoil.sh
COPY run.sh /usr/local/bin/run.sh
CMD /bin/bash /usr/local/bin/run.sh
