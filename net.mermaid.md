
```mermaid
flowchart TD
    A[ fa:fa-users Public IPv6/IPv4] -->| fa:fa-cloud TCP/80+443| B(Docker Port Expose)
    B --> C( fa:fa-desktop Host Machine )
    C -->|HTTP_S Traffic| D{ fa:fa-road NGINX_NET  }
    D -->|HTTP_S Traffic| E{ fa:fa-cubes NGINX}
    D -->|PORT 60000| H[ fa:fa-university NGINXUI]
    D --> F
    E -->|CACHE| F[ fa:fa-database REDIS]
    E -->| fa:fa-hdd-o CONFIGS| G( fa:fa-folder Shared Volumes)
    H -->| fa:fa-hdd-o CONFIGS | G   
    P[ fa:fa-lock Private IPv6/IPv4] -->|BIND_UI_IP_TCP/60000 DEFAULT: 127.0.0.1| C
    E -->|HTTP:your-syno.com:5001| S( fa:fa-floppy-o DISKSTATION)
    D --> S
    E -->|HTTPS:your-domain.com| X( fa:fa-linux YOUR_APP_2)
    D --> X
    E -->|HTTP:another-domain.net| Y( fa:fa-rss-square YOUR_APP_1)
    D --> Y
    E -->|HTTPS:ma-brother-domain.org| Z( fa:fa-play-circle YOUR_APP_3)
    D --> Z
```
